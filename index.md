# RedHat Advanced Cluster Security

## Índice

- [1. Definición](01_Main.md##Definición)
- [2. HLD Advanced Cluster security](02_HLD_Advanced_Cluster_Security.md)
- [3. Instalación](03_Instalación.md)
  - [3.1. Provided API](03_Instalación.md##Provided-API)
    - [3.1.1 Central](03_Instalación.md###Central)
    - [3.1.1 Secured Cluster](03_Instalación.md###Secured-Cluster)
  - [3.2. Instalación de Central](03_Instalación.md##Instalación-de-Central)
  - [3.3. Solución de Secured Cluster](03_Instalación.md##Instalación-de-Secured-Cluster)

<br> 

## Definición:
Red Hat Advanced Cluster Security for Kubernetes (RHACS) es una solución de seguridad de contenedores “kubernetes-native” que protege las aplicaciones vitales en la construcción, despliegue y tiempo de ejecución

Esta solución se despliega en la infraestructura y se integra con herramientas de DevOps, workflows para aportar mayor seguridad y cumplimiento a la infraestructura

<br>
